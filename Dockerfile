FROM tomcat

ADD build/libs/speechserver.war /usr/local/tomcat/webapps/speechserver

CMD ["catalina.sh", "run"]