package speechserver


class UserController {



    static allowedMethods = [enroll: "POST", index: "GET"]

    def index() {

    }

    def train(){

        def webrootDir = servletContext.getRealPath("/")


        def users = User.findAll();
        for(User u in users){
            File fileDest = new File(webrootDir,u.enrollmentFilePath)
            RecognizeManager.recognito.createVoicePrint(u, fileDest);
        }

        render "the model has been trained!"

    }

    def enroll(){

        def enrollmentPath = "enrollment_"+params.firstname+"_"+params.lastname+".wav";

        //remove old users with this firstName
        def oldUsers = User.findAllByEnrollmentFilePath(servletContext.getRealPath("/")+enrollmentPath);
        oldUsers*.delete(flush: true);

        //get the uploaded file
        def enrollmentFile = request.getFile('enrollmentFile')

        //save the file
        def webrootDir = servletContext.getRealPath("/")
        File fileDest = new File(webrootDir,enrollmentPath)
        enrollmentFile.transferTo(fileDest)

        //create a user record in the database
        User newUser = new User(firstName: params.firstname, lastName: params.lastname, enrollmentFilePath:enrollmentPath).save(flush:true)

        RecognizeManager.recognito.createVoicePrint(newUser, fileDest);

        render params.firstname+" "+params.lastname+" has been enrolled!"


    }

    def authenticate(){

        //get authentication file
        def authenticationFile = request.getFile("authenticationFile");

        //save file
        def webrootDir = servletContext.getRealPath("/")
        File fileDest = new File(webrootDir,"authentication_"+params.firstname+"_"+params.lastname+".wav")
        authenticationFile.transferTo(fileDest)

        List<MatchResult<User>> matches = RecognizeManager.recognito.identify(fileDest);
        MatchResult<User> match = matches.get(0);

        if(match){
            render match.getKey().firstName+" "+match.getKey().lastName
        }
        else{
            render "Couldnt Identify";
        }

    }

}
