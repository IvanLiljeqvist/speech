package speechserver

import org.bson.types.ObjectId

class User {

    static mapWith = "mongo"

    ObjectId id
    String firstName
    String lastName
    String enrollmentFilePath

}


